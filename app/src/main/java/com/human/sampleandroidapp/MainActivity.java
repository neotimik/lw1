package com.human.sampleandroidapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setContentView(new DrawView(this));
    }

    class DrawView extends View {
        Paint p;
        Rect rect;
        float[] points = new float[]{
                700, 650, 800, 1400,
                650, 650, 550, 1400
        };
        Random random = new Random();

        private Paint randColor(Paint p) {
            p.setColor(Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255)));
            return p;
        }

        public DrawView(Context context) {
            super(context);
            p = new Paint();
            rect = new Rect();
        }

        @Override
        protected void onDraw(Canvas canvas) {

            canvas.drawARGB(80, 102, 204, 255);
            p.setColor(Color.RED);
            canvas.drawRect(200, 150, 400, 200, p);
            p = randColor(p);
            canvas.drawCircle(100, 200, 50, p);
            p.setStrokeWidth(10);
            p.setColor(Color.rgb(128, 0, 128));
            canvas.drawRect(200, 300, 500, 600, p);

            canvas.drawLines(points, p);
            p = randColor(p);
            canvas.drawLine(555, 1350, 790, 1350, p);
            p = randColor(p);
            canvas.drawLine(575, 1250, 780, 1250, p);
            p = randColor(p);
            canvas.drawLine(590, 1150, 765, 1150, p);
            p = randColor(p);
            canvas.drawLine(600, 1050, 755, 1050, p);
            p = randColor(p);
            canvas.drawLine(610, 950, 740, 950, p);
            p = randColor(p);
            canvas.drawLine(625, 850, 725, 850, p);
            p = randColor(p);
            canvas.drawLine(635, 750, 710, 750, p);
            p = randColor(p);
            canvas.drawLine(645, 700, 705, 700, p);

        }
    }
}
